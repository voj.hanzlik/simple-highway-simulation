package cvut.fel;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import java.util.ArrayList;
import javafx.stage.Stage;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CarTest {






    @Test
    public void decideToChangeLaneTest_listFullOf1s_returnsTrue(){

        ArrayList<Integer> mockedProbabilityList = new ArrayList<>();
        for (int i = 0; i < 70; i++) {
            mockedProbabilityList.add(1);
        }

        final int[] Mockedlanes = {0, 1, 2, 3, 4};


        Car Mockedcar = new Car(0, 0, 0, 0, 0, 0,
                5, 1.5, 0, 1, 1, Mockedlanes);


        boolean expected = true;

        boolean result = Mockedcar.decideToChangeLane(mockedProbabilityList);

        assertEquals(expected, result);

    }

}
