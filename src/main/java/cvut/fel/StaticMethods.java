package cvut.fel;

import java.util.ArrayList;

public class StaticMethods {

    /**
     *  Loops through all objects in cars and checks if their bounding box intersects the given coords
     *
     * @param X         X in pixels
     * @param Y         Y in pixels
     * @param width     width in pixels
     * @param height    height in pixels
     * @param cars      list of all objects on the road
     * @return          true if the given coords are occupied, false otherwise
     */

    protected static boolean isOccupied(double X, double Y, double width, double height, ArrayList<Car> cars){
        for(Car car : cars){
            if(car.getY() == Y){ // On the same lane
                if (car.getBBox().intersects(X, Y, width, height)){
                    return true;
                }
            }
        }
        return false;
    }






}
