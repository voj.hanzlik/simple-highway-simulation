package cvut.fel;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;


public class Model {



	// Get the current screen resolution
	GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
	final double width = gd.getDisplayMode().getWidth();
	final double height = gd.getDisplayMode().getHeight();

	final double dt = 0.2;  // Randomly picked through trial end error so that the Intelligent Driver's Model works

    // Eyeball magic numbers so that the lanes stand for the right Y pixels
    final int firstLane = (int)(getHeight()/5.8);
    final int secondLane = (int)(getHeight()/3.9);
    final int thirdLane = (int)(getHeight()/2.95);
    final int fourthLane = (int)(getHeight()/2.4);
    final int fifthLane = (int)(getHeight()/2);
    final int[] lanes = {firstLane, secondLane, thirdLane, fourthLane, fifthLane};



	// Set the background size accordingly
	Background background = new Background(width/1.75, height/1.75);

    ArrayList<Car> cars = new ArrayList<Car>();
    String[] carSkins = {"car0.png","car1.png","car2.png","car3.png","car4.png",
            "car5.png","car6.png","car7.png","car8.png","car9.png","car10.png"};

    final double carWidth = 90;
    final double carHeight = 70;
    final double roadblockWidth = 70;
    final double roadblockHeight = 70;

    boolean placingMode = false;
    boolean carsLooping = true;
    int numOfRoadblocks = 0;


    int patience; // Key number to determine how likely a car is to change lane
    ArrayList<Integer> ProbabilityList; // A list of 70 1s and 0s, their ratio is determined by @patience

    int trafficDensity;

    private static final Logger logger = Logger.getLogger(Model.class.getName());


    public Model() {
        this.patience = 50;
        changeProbabilityList(patience);

        this.trafficDensity = 100;
        changeDensity(trafficDensity);
    }

    /**
     *  Method driving all the cars, manages vehicle looping or spawning new ones otherwise
     */

    public void advance() {
    	for (Car car : cars) {
            if(!car.isRoadblock) {
                if (car.getX() >= background.getWidth()) {
                	if (carsLooping) {
                		if (!StaticMethods.isOccupied(-200, car.getY(), car.getWidth() * 2, car.getHeight(), cars)) {
	                        car.moveToStart();
	                        break;
	                    }
                	} else {
                		cars.remove(car);
                        logger.info("Car removed due to reaching the scene border");
                		Car newCar = createCarAtStart();
                		if(newCar != null) {
                			cars.add(newCar);
                            logger.info("New car added to the scene");
                		}
                    	break;
                	}
	                    
                    
                } else {

                    Car targetCar = getCarInFront(car);
                    car.advance(targetCar, dt, ProbabilityList, cars);


                }
            }
        }
    }

    /**
     *  Argument is the current car that a target is needed for.
     *  returns null if nothing is in front of the current car. Otherwise returns an object in front of the current car.
     *
     * @param currCar   current car
     * @return          the object in front of currCar wheter it is a car or a roadblock, return null if nothing is found
     */

    private Car getCarInFront(Car currCar) {
    	Car targetCar = null; // returns null if no object is in front of the car
    	double minDistance = 0;
        for (Car car : cars){
        	if(car != currCar) {
	            if(car.getY() == currCar.getY()){ // If targetCar is on the same lane
	                if(car.getX() > currCar.getX()){ // If targetCar is in front of me

	                    double distance = car.getX() - currCar.getX();
	                    if(minDistance == 0){// If this targetCar is the first one being checked
	                        minDistance = distance;
	                        targetCar = car;
	                    }
	                    if(distance < minDistance){
	                        minDistance = distance;
	                        targetCar = car;
	                    }
	                }
	            }
        	}
        }
        return targetCar;
    }


    /**
     *  Takes the trafficDensity value and changes the number of generated cars accordingly.
     *
     * @param trafficDensity    value from the Density slider
     */


    public void changeDensity(double trafficDensity){
        int count = 0;
        int baseNumOfCars = 20;
        int requiredNumOfCars = (int)(baseNumOfCars * (trafficDensity/100));
        int currLen = cars.size();
        if(requiredNumOfCars - (currLen - numOfRoadblocks) > 0){
            while(cars.size()-numOfRoadblocks != requiredNumOfCars && count <= 60){
                Car newCar = createRandomCar();
                if(newCar != null) {
                    addCar(newCar);
                    logger.info("Random cars generated due to density change");
                }
                count++;

            }
        }else if(requiredNumOfCars - (currLen - numOfRoadblocks) < 0){
            while(cars.size()-numOfRoadblocks != requiredNumOfCars){
                removeRandCar();
                logger.info("Random cars removed due to density change");
            }
        }
    }


    private void removeRandCar(){
        Random random = new Random();
        int randIdx = random.nextInt(cars.size());
        if(!cars.get(randIdx).isRoadblock){
            cars.remove(randIdx);
        }
    }

    /**
     *  Creates a new instance of Car based on randomly selected values.
     *  Returns null if no free coordinates have been found
     *
     * @return null or an instance of Car
     */
    
    private Car createRandomCar(){
        Car newCar = null;
        Random random = new Random();
        int[] freeCoords = getFreeRandomCoords();
        if(freeCoords[1] != 0){
            double randSpeed = random.nextInt(5)+1;
            double randMaxAccel = random.nextInt(8-1)+1;
            double randMaxSpeed = randSpeed + random.nextInt(60-7)+7;
            int randSkinIdx = random.nextInt(11);

            newCar =  new Car(freeCoords[0], freeCoords[1], randSpeed, 0, randMaxAccel, randMaxSpeed,
                    5, 1.5, randMaxAccel +3, carWidth, carHeight, lanes, carSkins[randSkinIdx]);
        }
        return newCar;
    }

    /**
     *  Loops through random coordinates and checks if they are free.
     *
     * @return Returns unoccupied coordinates. Returns 0 at coord Y if no free coords have been found
     *
     */

    private int[] getFreeRandomCoords(){
        int count = 0;
        boolean isOccupied = true;
        int[] freeCoords = {0,0};
        Random random = new Random();
        while(isOccupied && count < 60){
            int randX = random.nextInt((int)background.getWidth());
            int randY = random.nextInt(lanes.length);
            randY = lanes[randY];
            freeCoords[0] = randX;
            freeCoords[1] = randY;
            isOccupied = StaticMethods.isOccupied(randX, randY, 90, 70, cars);
            count++;
        }
        if(isOccupied){
            freeCoords[1] = 0;
        }
        return freeCoords;
    }

    /**
     *  Changes the ratio of 1s and 0s in the Probability list based on the argument
     *
     * @param percentage    Value from the Drivers patience slider
     */

    public void changeProbabilityList(double percentage){
        percentage = (percentage - 100) * (-1); // Converts the patience slider value --> 0 patience = 100% likelihood to change lane
        ProbabilityList = new ArrayList<Integer>();
        int numOfNumbers = 70;
        int numOf1s = (int)((percentage/100) * numOfNumbers);
        int numOf0s = numOfNumbers - numOf1s;

        for(int i = 0; i < numOf1s ;i++){
            ProbabilityList.add(1);
        }
        for(int i = 0; i < numOf0s ;i++){
            ProbabilityList.add(0);
        }
    }

    /**
     *  Creates an instance of Car as a roadblock if the argument coordinates are unoccupied
     *
     * @param x     X pixels
     * @param y     Y pixels
     */

    protected void placeRoadblock(double x, double y){

        if(placingMode) {
            y = getLaneBasedOnY(y);
            if (!StaticMethods.isOccupied(x, y, roadblockWidth, roadblockHeight,cars)) {
                if (y != 0) {
                    addCar(new Car(x, y, roadblockWidth, roadblockHeight, 0));
                    logger.info("Roadblock placed");
                    numOfRoadblocks++;
                }
            }
        }
        placingMode = false;

    }

    /**
     *  Deletes an instance of Car as a roadblock if the argument coordinates are occupied by a roadblock
     *
     * @param x     X pixels
     * @param y     Y pixels
     */

    protected void removeRoadblock(double x, double y) {
        y = getLaneBasedOnY(y);
    	if(!placingMode) {
    		if(StaticMethods.isOccupied(x, y, roadblockWidth, roadblockHeight, cars)) {
    			for(Car car : cars) {
    				if(car.isRoadblock) {
    					if (car.getBBox().contains(x, y)){
    						cars.remove(car);
                            logger.info("Roadblock removed");
                            numOfRoadblocks--;
    						return;
    					}
    				}
    			}
    		}
    	}
    }

    /**
     *  Returns lane that has the argument in its boundaries
     *
     * @param y     Y position in pixels
     * @return      lane that has the argument in its boundaries
     */

    // Carefully measured magic numbers that set boundaries of each lane
    private double getLaneBasedOnY(double y){
        double retLane;
        if(firstLane - 29 <= y && y <= firstLane + 81){
             retLane = firstLane;
        }else if(secondLane - 31 <= y && y <= secondLane + 82){
            retLane = secondLane;
        }else if(thirdLane - 25 <= y && y <= thirdLane + 73){
            retLane = thirdLane;
        }else if(fourthLane - 29 <= y && y <= fourthLane + 86){
            retLane = fourthLane;
        }else if(fifthLane - 26 <= y && y <= fifthLane + 83){
            retLane = fifthLane;
        }else{
            retLane = 0;
        }

        return retLane;
    }

    private int[] getFreeCoordsAtStart() {
    	boolean isOccupied = true;
    	int[] freeCoords = {0,0};
    	Random random = new Random();
    	int X = -200; // Starting x coord
    	int count = 0;
    	freeCoords[0] = X;
    	freeCoords[1] = 0; // Stays 0 if no free lane is found
        while(isOccupied && count != 10){ // count ensures that the while loop doesnt loop infinitely in case all lanes are busy
            int randY = random.nextInt(lanes.length);
            randY = lanes[randY];
            freeCoords[1] = randY;
            isOccupied = StaticMethods.isOccupied(X, randY, 90*2, 70, cars);
            count++;
        }
        if (isOccupied == true){
            freeCoords[1] = 0;
        }
        return freeCoords;
    }
    
    private Car createCarAtStart(){
    	Car newCar = null;
        Random random = new Random();
        int[] freeCoords = getFreeCoordsAtStart();
        if(freeCoords[1] != 0) {
        	double randSpeed = random.nextInt(5)+1;
        
        	double randMaxAccel = random.nextInt(7-1)+1;
        	double randMaxSpeed = randSpeed + random.nextInt(22-3)+3;
            int randSkinIdx = random.nextInt(11);

        	newCar = new Car(freeCoords[0], freeCoords[1], randSpeed, 0, randMaxAccel, randMaxSpeed,
        					 5, 1.5, randMaxAccel +3, carWidth, carHeight, lanes, carSkins[randSkinIdx]);
        }
        return newCar;
    }
    

    public ArrayList<Car> getCars(){
        return cars;
    }


    public Background getBackground() {
        return background;
    }

    public void addCar(Car car) {
    	cars.add(car);
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }
}





