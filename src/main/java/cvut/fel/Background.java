package cvut.fel;

import javafx.scene.image.Image;

public class Background {

    double width;
    double height;
    Image skin;

    /**
     * Constructs an instance of Background.
     * arguments specify the size of the image
     *
     * @param width specifies the width of the image
     * @param height specifies the height of the image
     */

    public Background(double width, double height) {
        this.width = width;
        this.height = height;
        this.skin = new Image("background.png", width, height, false, false);

    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public Image getSkin(){
        return skin;
    }
}
