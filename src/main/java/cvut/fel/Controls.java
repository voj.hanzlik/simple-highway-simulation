package cvut.fel;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.util.logging.Logger;

public class Controls {
    private final View view;
    private final Model model;

    private static final Logger logger = Logger.getLogger(Controls.class.getName());


    public Controls(View view, Model model) {
        this.view = view;
        this.model = model;
    }


    /**
     *  Handles the density slider
     */

    ChangeListener<Number> densitySliderHandler = new ChangeListener<Number>() {
        public void changed(ObservableValue<? extends Number> ov,
                            Number old_val, Number new_val) {
            if(new_val.doubleValue() == 0 || new_val.doubleValue() % 25 == 0){
                model.changeDensity(new_val.doubleValue());
            }
        }
    };

    /**
     * Handles the probability slider
     */
    ChangeListener<Number> probabilitySlider = new ChangeListener<Number>() {
        public void changed(ObservableValue<? extends Number> ov,
                            Number old_val, Number new_val) {
            if(new_val.doubleValue() == 0 || new_val.doubleValue() % 25 == 0){
                model.changeProbabilityList(new_val.doubleValue());
            }


        }
    };

    /**
     * Handles the create button
     */
    EventHandler<ActionEvent> roadblockButtonHandler = new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            model.placingMode = true;
            logger.info("Roadblock button clicked");
        }
    };


    /**
     * Handles mouseclicks,
     * places roadblock if the create button has been pressed,
     * removes it otherwise
     */
    EventHandler<MouseEvent> roadblockEventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            if (e.getSceneY() > (model.firstLane - 29)) {
                if (model.placingMode) {
                    model.placeRoadblock(e.getSceneX(), e.getSceneY());
                } else if (model.placingMode == false) {
                    model.removeRoadblock(e.getSceneX(), e.getSceneY());
                }


            }
        }
    };

    /**
     * Enables or disables vehichle looping
     */

    EventHandler<ActionEvent> carLoopHandler = new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            if(model.carsLooping) {
            	model.carsLooping = false;
            }else {
            	model.carsLooping = true;
            }
        }
    };

}
