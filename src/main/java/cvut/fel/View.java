package cvut.fel;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.logging.Logger;

public class View {

    private final Stage stage;
    private Canvas canvas;
    protected Pane pane;

    private final Model model;


    Slider densitySlider;
    Slider probabilitySlider;
    Button Roadblockbutton;
    Button carLoopButton;

    private static final Logger logger = Logger.getLogger(View.class.getName());

    public View(Stage stage, Model model) {
        this.stage = stage;
        this.model = model;

    }

    /**
     * Creates a canvas, pane, scene and finally the whole stage of the application
     */

    public void initDrawingSurface() {
        canvas = new Canvas(model.getBackground().getWidth(), model.getBackground().getHeight());
        pane = new Pane(canvas);
        addButtons(pane);
        Scene scene = new Scene(pane);
        stage.setTitle("Highway Traffic");
        stage.setWidth((model.getWidth()/1.75));
        stage.setHeight((model.getHeight()/1.75));
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
        logger.info("Stage initialized");
    }

    /**
     * Updates the canvas
     */

    public void render() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        // Clear the canvas before drawing updated state
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        drawBackground(gc);
        drawCars(gc);
    }



    private void drawBackground(GraphicsContext gc) {
        gc.drawImage(model.getBackground().getSkin(), 0, 0);
    }

    private void drawCars(GraphicsContext gc) {
        ArrayList<Car> cars = model.getCars();
        for (int i = 0; i < cars.size(); i++) {
            gc.drawImage(cars.get(i).getSkin(), cars.get(i).getX(), cars.get(i).getY());
        }

    }


    /**
     *  Adds buttons and sliders to the pane
     *
     * @param pane  desired pane for the button placement
     */

    protected void addButtons(Pane pane){
        double currWidth = model.getWidth() / 100;

        densitySlider = new Slider( 0, 200, 100);
        densitySlider.setBlockIncrement(25);
        densitySlider.setMajorTickUnit(25f);
        densitySlider.setMinorTickCount(0);
        densitySlider.setShowTickLabels(true);
        densitySlider.setSnapToTicks(true);
        densitySlider.setPrefSize(400,40);
        densitySlider.relocate(currWidth, 90);

        Label label1 = new Label("Number of generated vehicles in %. Default number is 20.");
        label1.relocate(currWidth+60, 70);


        probabilitySlider = new Slider( 0, 100, 50);
        probabilitySlider.setBlockIncrement(25);
        probabilitySlider.setMajorTickUnit(25f);
        probabilitySlider.setMinorTickCount(0);
        probabilitySlider.setShowTickLabels(true);
        probabilitySlider.setSnapToTicks(true);
        probabilitySlider.setPrefSize(200,40);
        probabilitySlider.relocate(currWidth+500, 90);

        Label label2 = new Label("Drivers' Patience");
        label2.relocate(currWidth+560, 70);

        Roadblockbutton = new Button("Create Roadblock");
        Roadblockbutton.relocate(currWidth+900, 70);

        carLoopButton = new Button("Turn on/off vehicle loop");
        carLoopButton.relocate(currWidth+883, 110);

        pane.getChildren().addAll(densitySlider,label1,probabilitySlider,label2,Roadblockbutton,carLoopButton);
        logger.info("Buttons added to the scene");
    }

}
