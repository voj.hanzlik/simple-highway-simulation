package cvut.fel;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


// Majority of this class has been shamelessly taken from the Tower Defense PJV example
public class App extends Application {

    private Model model;
    private View view;
    private AnimationTimer gameLoopTimer;
    private Controls controls;

    @Override
    public void start(Stage stage) {

        model = new Model();
        view = new View(stage, model);
        controls = new Controls(view, model);

        view.initDrawingSurface();


        view.densitySlider.valueProperty().addListener(controls.densitySliderHandler);
        view.probabilitySlider.valueProperty().addListener(controls.probabilitySlider);
        view.Roadblockbutton.setOnAction(controls.roadblockButtonHandler);
        view.pane.addEventFilter(MouseEvent.MOUSE_CLICKED, controls.roadblockEventHandler);
        view.carLoopButton.setOnAction(controls.carLoopHandler);


        gameLoopTimer = new AnimationTimer() {
            private long lastUpdate = 0;

            @Override
            public void handle(long now) { // in nanoseconds
                if (now - lastUpdate >= 100_000_00) {
                    // render
                    view.render();
                    // update
                    model.advance();
                    lastUpdate = now;
                }
            }
        };
        gameLoopTimer.start();


    }

    public static void main(String[] args) {
        launch();
    }

}