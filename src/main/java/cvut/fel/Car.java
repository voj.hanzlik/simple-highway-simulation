package cvut.fel;

import javafx.geometry.BoundingBox;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.Random;
import java.lang.Math;
import java.util.logging.Logger;

public class Car {


    double x;
    double y;
    double width;
    double height;
    BoundingBox BBox;
    double speed;
    double acceleration;
    double maxAcceleration;
    double maxSpeed;
    Image skin;
    
    double s0;
    double T;
    
	
	double maxDeceleration;
	double sqrt_ab;
	
    boolean isRoadblock;

    int[] lanes;


    /**
     * Constructs an instance of a Car.
     * This particular instance is used to create a vehicle, not a roadblock!
     *
     * @param x                 X pixel position of the car
     * @param y                 Y pixel position of the car
     * @param speed             number of pixels to determine the upcoming X position of the car
     * @param acceleration      number of pixels to determine the change of speed
     * @param maxAcceleration   number which specifies the maximum value of acceleration
     * @param maxSpeed          number which specifies the maximum value of speed
     * @param s0                number of pixels to specify minimum gap between the car and and object in front
     * @param T                 reaction time of the car, used in the IDM equation
     * @param maxDeceleration   number which specifies the maximum value of deceleration
     * @param width             number to specify width of image of the car in pixels
     * @param height            number to specify height of image of the car in pixels
     * @param lanes             list of all lanes on the road where the car is at
     * @param skin              image of the car, argument is the URL of the image
     */

    public Car(double x, double y, double speed, double acceleration, double maxAcceleration,double maxSpeed, double s0, double T, double maxDeceleration, double width, double height, int[] lanes, String skin) {
		this.x = x;
		this.y = y;
        this.width = width;
        this.height = height;
		this.BBox = new BoundingBox(x, y, width, height);
		this.speed = speed;
		this.acceleration = acceleration;
        this.maxAcceleration = maxAcceleration;
		this.maxSpeed = maxSpeed;
		this.skin = new Image(skin, width, height, true, false);
		this.s0 = s0;
		this.T = T;
		this.maxDeceleration = maxDeceleration;
		this.sqrt_ab = 2 * Math.sqrt(maxAcceleration * maxDeceleration);
        this.isRoadblock = false;
        this.lanes = lanes;
	}
    // Constructor used for testing purposes
    public Car(double x, double y, double speed, double acceleration, double maxAcceleration,double maxSpeed, double s0, double T, double maxDeceleration, double width, double height, int[] lanes) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.BBox = new BoundingBox(x, y, width, height);
        this.speed = speed;
        this.acceleration = acceleration;
        this.maxAcceleration = maxAcceleration;
        this.maxSpeed = maxSpeed;
        this.s0 = s0;
        this.T = T;
        this.maxDeceleration = maxDeceleration;
        this.sqrt_ab = 2 * Math.sqrt(maxAcceleration * maxDeceleration);
        this.isRoadblock = false;
        this.lanes = lanes;
    }

    /**
     * Constructs an instance of a Car.
     * This particular instance is used to create a ROADBLOCK! not a car
     *
     * @param x         X pixel position of the roadblock
     * @param y         Y pixel position of the roadblock
     * @param width     number to specify width of image of the roadblock in pixels
     * @param height    number to specify height of image of the roadblock in pixels
     * @param speed     is always 0 for roadblock, important for the IDM equation
     */

    // Used to create a roadblock
    public Car(double x, double y, double width, double height, double speed) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.BBox = new BoundingBox(x, y, width, height);
        this.speed = speed;
        this.skin = new Image("roadblock.png", width, height, false, false);
        this.isRoadblock = true;
    }

    /**
     * Implementation of the Intelligent Driver's Model.
     * Calculates acceleration to be changed in the next step.
     * Majority of this method is based on <a href="https://towardsdatascience.com/simulating-traffic-flow-in-python-ee1eab4dd20f">this article</a>
     * Please see <a href="https://traffic-simulation.de/info/info_IDM.html">this link</a>
     * or <a href="https://en.wikipedia.org/wiki/Intelligent_driver_model">Wikipedia</a>
     * for more detailed information about the IDM.
     *
     * @param targetCar         can either be a car in front of the current car
     * @param dt                time step, the right value was achieved through trial and error
     * @param ProbabilityList   list of 0s and 1s, their ration is based on Model, needed to determin whether the car changes lane or not
     * @param cars              list of all objects on the road, needed for lane change
     */

    public void advance(Car targetCar, double dt, ArrayList<Integer> ProbabilityList, ArrayList<Car> cars) {
    	
    	if(speed + acceleration*dt < 0) {
    		x -= 1/2*speed*speed/acceleration;
    		speed = 0;
    		
    	} else {
    		speed += acceleration * dt;
    		x += speed*dt + acceleration*dt*dt/2;
    	}
    	
    	double alpha = 0;
    	if (targetCar != null) {
            double delta_x = targetCar.getX() - (x + width);
    		double delta_v = speed - targetCar.getSpeed();

            if(delta_x < 30){
                tryLaneChange(ProbabilityList, cars);
            }

    		alpha = (s0 + Math.max(0, T * speed + delta_v * speed / sqrt_ab)) / delta_x;
    	}
    	
    	acceleration = maxAcceleration * (1 - Math.pow((speed / maxSpeed), 4) - Math.pow(alpha, 2));

    	
    	this.BBox = updateBBox(x, y, this.width, this.height);

    }

    /**
     *  Checks if the car decides to change lane based on the Probability list, then checks if the adjacent lanes are free
     *  then changes lane or not accordingly
     *
     * @param ProbabilityList   list of 0s and 1s, their ration is based on Model, needed to determin whether the car changes lane or not
     * @param cars              list of all objects on the road, needed for lane change
     *
     */

    private void tryLaneChange(ArrayList<Integer> ProbabilityList, ArrayList<Car> cars){
        if(decideToChangeLane(ProbabilityList)){ // If car decides to change lane
            int possibleLaneIdx = chooseLaneIdx(); // determine index of an adjacent lane
            if(StaticMethods.isOccupied(getX()-40, lanes[possibleLaneIdx], getWidth()+60, getHeight(), cars)) { // check if the lane is free
                int currIdx = getIdxOfCurrLane(getY());
                if(currIdx > 0 && currIdx < 4){ // If the car is not in one of the outer lanes
                    if(!StaticMethods.isOccupied(getX()-40, lanes[possibleLaneIdx+2], getWidth()+40, getHeight(), cars)) { // check for space in the other adjacent lane
                        moveCarToLane(possibleLaneIdx+2);

                    }
                }

            }else {
                moveCarToLane(possibleLaneIdx);

            }
        }
    }

    /**
     *  Updates bounding box of the car based on given arguments
     *
     * @param x         X position
     * @param y         Y position
     * @param width     width in pixels
     * @param height    height in pixels
     * @return          updated Bounding box
     */

    public BoundingBox updateBBox(double x, double y, double width, double height) {
    	return new BoundingBox(x, y, width, height);
    }

    /**
     *  Sets Y position based on the argument
     *
     * @param laneIdx   index of the desired lane in the lanes list
     */

    protected void moveCarToLane(int laneIdx){
        setY(lanes[laneIdx]);
        updateBBox(getX(), getY(), getWidth(), getHeight());
    }

    /**
     *  Sets current X position so that the car is moved to the beginning of the scene
     */

    public void moveToStart(){
        setX(-200);
        this.BBox = updateBBox(-200, y, width, height);
    }


    /**
     *  Argument is the Probability list from class Model
     *
     * @param list  list of 0s and 1s, their ration is based on Model, needed to determin whether the car changes lane or not
     * @return      true if the randomly selected number is 1, false if the number is 0
     */

    // Randomly picks an index from given list. The list is expected to consist of 1s and 0s
    protected boolean decideToChangeLane(ArrayList<Integer> list){
        Random random = new Random();
        int randomIndex = random.nextInt(list.size());
        if(list.get(randomIndex) == 1){
            return true;
        }else{
            return false;
        }
    }

    /**
     *  Selects and adjacent lane index baseed on the current one
     *
     * @return  index of the adjacent lane
     */

    protected int chooseLaneIdx(){
        double currLane = getY();
        int currIdx = getIdxOfCurrLane(currLane);
        int chosenLaneIdx;

        switch (currIdx){
            case 0:
                chosenLaneIdx = 1;
                break;
            case 4:
                chosenLaneIdx = 3;
                break;
            default:
                chosenLaneIdx = currIdx - 1;
        }
        return chosenLaneIdx;
    }

    /**
     *  Returns index of the current lane based on current lane
     *
     * @param lane  current lane
     * @return      index of current lane
     */

    protected int getIdxOfCurrLane(double lane){
        int idx = 0;
        for (int i = 0; i < lanes.length; i++) {
            if(lane == lanes[i]){
                idx = i;
                break;
            }
        }
        return idx;
    }



    public Image getSkin() {
        return skin;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getSpeed() {
        return speed;
    }

    public BoundingBox getBBox() {
        return BBox;
    }

    public void setY(double y) {
        this.y = y;
    }

}
